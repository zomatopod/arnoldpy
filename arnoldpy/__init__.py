import pathlib

with pathlib.Path(__file__).parent.joinpath("VERSION").open('r') as ver_file:
    __version__ = ver_file.read().strip()

from arnoldpy.cmdline import main as run_cmdline
