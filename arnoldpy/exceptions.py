"""Exceptions used by :mod:`arnoldpy`"""

from arnoldpy import nodes


class ArnoldPyException(Exception):
    """Base for all :mod:`ArnoldPy` exceptions."""


class ParseError(ArnoldPyException):
    """Critical error during parsing."""

    def __init__(self, info, message):
        # Info is either a parseinfo or lineinfo
        try:
            pos = info.pos
            lineinfo = info.buffer.line_info(pos)
        except AttributeError:
            lineinfo = info

        super().__init__(
            str(nodes.Type.error.value) + " " +
            "({line}:{column}) {msg}\n\n{code}\n{caret_ws}^\n".format(
                line=lineinfo.line+1,
                column=lineinfo.col+1,
                msg=message,
                code=lineinfo.text.replace("\t", " ").rstrip(),
                caret_ws=" "*lineinfo.col,
            )
        )


class AnalysisError(ParseError):
    """Error during semantic analysis. These errors are not necessarily
    Python errors (e.g. Python doesn't require variable declaration),
    but are invalid ArnoldC.
    """
