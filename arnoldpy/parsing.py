"""The primary parsing module of :mod:`ArnoldPy` to build an abstract
syntax tree from an ArnoldC source.

Generated trees are generic (based on :class:`grako.ast.AST` objects),
with a known structure and set of components, allowing for single
implementation of analysis and other transformations. In order to
produce a tree for a target language, "foreign payloads" are attached to
each node, allowing a second tree to be built side-by-side with the
default, generic AST.

Foreign generation of Python ASTs are performed with :mod:`arnoldpy.
python`.
"""

import enum
import functools

import grako.buffering
import grako.exceptions
from grako.contexts import Closure
from grako.ast import AST

from arnoldpy import exceptions
from arnoldpy import nodes
from arnoldpy import analysis
from arnoldpy.base import ArnoldParser, ArnoldSemantics


class ArnoldCMacros(enum.Enum):
    """Enum of ArnoldC @ "macro" constants."""

    true = ("NO PROBLEMO", "1")
    false = ("I LIED", "0")


class ArnoldCBuiltin(enum.Enum):
    """Enum of ArnoldC builtin methods."""

    read_input = ("I WANT TO ASK YOU A BUNCH OF QUESTIONS AND I WANT TO HAVE "
                  "THEM ANSWERED IMMEDIATELY")


class Parser(ArnoldParser):
    """ArnoldPy parser. Parses ArnoldC source text into an abstract
    syntax tree.
    """

    ROOT_RULE = "program"

    def __init__(self, semantics=None):
        if semantics is None:
            semantics = Semantics()
        super().__init__(
            parseinfo=True,
            whitespace=" \t",
            semantics=semantics
        )

    def parse(self, text, filename=None, semantics=None, trace=False):
        """Parse ArnoldC *text* string and return the parsed AST.

        Validation is done on the parsed root node before it is
        returned. Any parsing or semantic problems will raise
        :exc:`grako.exceptions.ParseError`.

        Note that unlike the base :meth:`grako.parsing.Parser.parse`,
        *text* cannot be a :class:`grako.buffering.Buffer`.
        """

        try:
            program_ast = super().parse(
                Buffer(text),
                self.ROOT_RULE,
                filename=filename,
                semantics=semantics,
                trace=trace
            )
        except grako.exceptions.FailedParse as err:
            raise exceptions.ParseError(
                err.buf.line_info(err.pos),
                err.message
            )

        # Static semantic analysis.
        nodes.walk(
            program_ast,
            analysis.MethodReturns(),
            analysis.VariableDeclaration()
        )

        if program_ast._foreign_payload_:
            program_ast['foreign_payload'] = program_ast._foreign_payload_
        return program_ast


class Buffer(grako.buffering.Buffer):
    def __init__(self, text, filename=None, tabwidth=None, comments_re=None,
                 trace=False, nameguard=None):
        text = self.preprocess(text)
        super().__init__(text, filename, " \t", tabwidth, comments_re,
                         False, trace, nameguard)

    def preprocess(self, text):
        """Preprocess input source (e.g. expand @ macros)."""

        # This is grossly inefficient, but it'll do for now
        for enm in ArnoldCMacros:
            name, value = enm.value
            text = text.replace("@" + name, value)
        return text


def forgen(func):
    """The :func:`forgen` decorator simplifies the creation of foreign
    AST nodes in a :mod:`grako` semantics class.

    Foreign nodes are created by piggybacking onto the base AST
    generated by :class:`arnoldpy.parsing.Semantics`. For each node, a
    :attr:`_foreign_payload_` payload is attached, which contains the
    foreign AST associated with it. However, utilizing this creates a
    rather cumbersome boilerplate:

        import ast as python_ast

        def program(self, ast):
            super().program(ast)
            self.info.version = ast['version']._foreign_payload_
            methods = [method._foreign_payload_ for method in ast['methods']]
            ast._foreign_payload_ = python_ast.Module(methods)
            return ast

    :func:`forgen` allows this node generation to be cleaner and more
    direct:

        import ast as python_ast

        @forgen
        def program(self, ast):
            self.info.version = ast['version']
            methods = ast['methods']
            return python_ast.Module(methods)

    The decorator automatically calls the superclass method, extracts
    ``_foreign_payload_`` information from the AST node's fields to pass
    as the *ast* argument, and then sets the results to the host AST's
    ``_foreign_payload_``.

    """

    @functools.wraps(func)
    def wrapper(self, ast, *args, **kwargs):
        # Call superclass's method.
        getattr(super(self.__class__, self), func.__name__)(ast)

        # From Grako documentation:
        #
        #     "When there are no named items in a rule, the AST
        #     consists of the elements parsed by the rule, either
        #     a single item or a list. This default behavior makes
        #     it easier to write simple rules."
        #
        # Normally, the elements for a production rule exist as children
        # in *ast*, via named attributes or keys (Grako allows for
        # either access). But if the rule has no named elements, Grako
        # coereces the element(s) as *ast* itself.

        ret = None

        # Terminal rule with no named elements. This is the only
        # instance where we get a raw string instead of an AST.
        if isinstance(ast, str):
            ret = func(self, ast, *args, **kwargs)

        # Nonterminal rule consisting of a single unnamed element.
        # See comments above.
        elif isinstance(ast, AST) and ast._foreign_payload_:
            ret = func(self, _foreign_payload_value(ast), *args, **kwargs)

        # Terminal and nonterminal rules consisting of multiple unnamed
        # elements. See the comments above.
        elif isinstance(ast, list):
            ret = func(self, _foreign_payload_value(ast), *args, **kwargs)

        # All other terminal and nonterminal rules with named elements.
        elif isinstance(ast, AST):
            attrs = _NoneDict()
            for key, value in ast.items():
                attrs[key] = _foreign_payload_value(value)
            ret = func(self, attrs, *args, **kwargs)

        else:
            # Should not get here.
            raise TypeError("unknown AST input in '{}': {}"
                            .format(func.__name__, ast))

        # Set payload.
        if ret is not None:
            if ast._foreign_payload_:
                # Necessary because Grako ASTs appends values to an
                # attribute, creating a list, when using assignment. We
                # want to replace the payload, not add to it. A payload
                # should only preexist for nonterminals with a single
                # unnamed element (conditional #2).
                del ast['_foreign_payload_']
            ast._foreign_payload_ = ret
        elif not ast._foreign_payload_:
            ast._foreign_payload_ = True

        # Return the node with _foreign_payload_ payload back to the
        # parser.
        return ast

    def _foreign_payload_value(value):
        if isinstance(value, list):
            return list([_foreign_payload_value(v) for v in value])
        elif isinstance(value, AST):
            return value._foreign_payload_
        else:
            return value

    class _NoneDict(object):
        """Returns ``None`` when a key does not exist, similar in
        behavior to :class:`grako.ast.AST`.
        """

        def __init__(self):
            self._dict = {}

        def __getitem__(self, key):
            try:
                return self._dict[key]
            except KeyError:
                return None

        def __setitem__(self, key, value):
            self._dict[key] = value

        def clear(self):
            self._dict.clear()

        def __repr__(self):
            return repr(self._dict)

    return wrapper


def make_autotype(nodetype_enum, base=None):
    """Use as a dectorator to automatically apply node types to incoming
    ASTs from the parser in semantic class methods.

        from arnoldpy import nodes

        class NodeTypes(Enum):
            statement = 100

        def BaseSemantics(object):
            def statement(self, ast):
                return ast

        def Semantics(BaseSemantics):
            @make_autotype(NodeTypes, BaseSemantics)
            def statement(self, ast):
                nodes.istype(ast, NodeTypes.statement)       # True
                return ast

    *nodetype_enum* is the :class:`enum.Enum` to retrieve the the node
    type from. Each method's name is used as a key for the enumeration,
    and the received enum value is then applied to the AST using
    :func:`nodes.addtype`. An :exc:`AttributeError` is raised if the the
    type is not found when the decorator is applied.

    *base* is an optional semantic class to use as a sanity check.
    :exc:`AttributeError` is raised if an autotyped method does not
    match a semantic rule from the base class, also when the decorator
    is applied.
    """

    def autotype(func):
        if base and func.__name__ not in vars(base):
            raise AttributeError(
                "autotype: {} is not a method in base class {}"
                 .format(func, base)
            )
        try:
            nodetype = nodetype_enum[func.__name__]
        except KeyError:
            raise AttributeError(
                "autotype: '{}' is not a type defined in {}"
                 .format(func.__name__, nodetype_enum)
            )
        @functools.wraps(func)
        def wrapper(self, ast):
            nodes.addtype(ast, nodetype)
            return func(self, ast)
        return wrapper

    return autotype


class ForeignGeneration(type):
    """The :class:`ForeignGeneration` metaclass automatically applies
    :class:`forgen` decorators to the appropriate semantic methods.
    """

    def __new__(mcs, name, bases, namespace):
        # Do not apply to the base classes that lays down the work that
        # foreign generation actually piggybacks onto, i.e. topmost
        # classes and immediate descendants of ArnoldSemantics (which is
        # a no-op and can be disregarded). The idea is to apply forgens
        # onto the *children* of these base classes.
        bases = tuple(base for base in bases if base != ArnoldSemantics)
        if len(bases) > 0:
            for attr_name, value in namespace.items():
                # Only wrap public attributes that exist in the base
                # ArnoldSemantics.
                if (attr_name in vars(ArnoldSemantics)
                        and not attr_name.startswith("_")):
                    namespace[attr_name] = forgen(value)

        return super().__new__(mcs, name, bases, namespace)


class Semantics(ArnoldSemantics, metaclass=ForeignGeneration):
    """Default semantics for the ArnoldC parser. Produces a mostly-
    unaltered :mod:`grako` AST from the base grammer with assertions for
    correct grammar parsing, mostly useful for tests, a reference for
    new implementations, and input validation.

    Classes that inherit from :class:`Semantics` will receive and apply
    foreign payloads onto the base AST instead of replacing the
    individual nodes.
    """

    autotype = make_autotype(nodes.Type, ArnoldSemantics)


    # Reserved keywords that cannot be parsed as identifiers.
    # RESERVED = {
    # }

    #### STRUCTURE #####################################################

    @autotype
    def program(self, ast):
        assert "methods" in ast
        methods = ast['methods']
        assert isinstance(methods, Closure)
        assert all((nodes.istype(meth, nodes.Type.method)
                    for meth in methods))
        return ast

    @autotype
    def method(self, ast):
        assert (nodes.istype(ast, nodes.Type.method_main) or
                nodes.istype(ast, nodes.Type.method_defined))
        return ast

    @autotype
    def method_main(self, ast):
        assert "body" in ast
        body = ast['body']
        assert nodes.istype(body, nodes.Type.statement_body)
        return ast

    @autotype
    def method_defined(self, ast):
        assert "ident" in ast
        assert "parameters" in ast
        assert "body" in ast
        assert "nonvoid" in ast
        ident = ast['ident']
        parameters = ast['parameters']
        body = ast['body']
        nonvoid = ast['nonvoid']
        assert nodes.istype(ident, nodes.Type.identifier)
        assert nodes.istype(body, nodes.Type.statement_body)
        assert (nodes.istype(nonvoid, nodes.Type.nonvoid_flag) or
                nonvoid is None)
        assert isinstance(parameters, list) or parameters is None

        if parameters:
            assert all((nodes.istype(param, nodes.Type.parameter)
                        for param in parameters))
        return ast

    @autotype
    def nonvoid_flag(self, ast):
        assert "flag" in ast
        return ast

    @autotype
    def parameter(self, ast):
        assert "ident" in ast
        ident = ast['ident']
        assert nodes.istype(ident, nodes.Type.identifier)
        return ast

    #### ATOMS #########################################################

    @autotype
    def identifier(self, ast):
        assert "name" in ast
        assert isinstance(ast['name'], str)
        return ast

    @autotype
    def builtin_identifier(self, ast):
        assert "name" in ast
        assert isinstance(ast['name'], str)
        try:
            ArnoldCBuiltin(ast['name'])
        except ValueError:
            raise grako.exceptions.FailedSemantics("invalid builtin method")
        return ast

    @autotype
    def number(self, ast):
        assert "num" in ast
        assert isinstance(ast['num'], str)
        # Could possibly test if the string actually represents an int.
        return ast

    @autotype
    def print_string(self, ast):
        assert "string" in ast
        assert isinstance(ast['string'], str)
        return ast

    #### STATEMENTS ####################################################

    @autotype
    def statement_body(self, ast):
        assert "statements" in ast
        statements = ast['statements']
        assert isinstance(statements, Closure)
        assert all((nodes.istype(stmt, nodes.Type.statement)
                    for stmt in statements))
        return ast

    @autotype
    def statement(self, ast):
        stmt_types = {
            nodes.Type.stmt_print,
            nodes.Type.stmt_declaration,
            nodes.Type.stmt_assignment,
            nodes.Type.stmt_conditional,
            nodes.Type.stmt_while,
            nodes.Type.stmt_call_method,
            nodes.Type.stmt_return
        }
        assert any((nodes.istype(ast, st) for st in stmt_types)), ast
        return ast

    @autotype
    def stmt_declaration(self, ast):
        assert "ident" in ast
        assert "value" in ast
        ident = ast['ident']
        value = ast['value']
        assert nodes.istype(ident, nodes.Type.identifier)
        assert nodes.istype(value, nodes.Type.expression)
        return ast

    @autotype
    def stmt_assignment(self, ast):
        assert "ident" in ast
        assert "operation" in ast
        ident = ast['ident']
        operation = ast['operation']
        assert nodes.istype(ident, nodes.Type.identifier)
        assert nodes.istype(operation, nodes.Type.operation_stack)
        return ast

    @autotype
    def stmt_print(self, ast):
        assert "value" in ast
        value = ast['value']
        assert (nodes.istype(value, nodes.Type.expression) or
                nodes.istype(value, nodes.Type.print_string))
        return ast

    @autotype
    def stmt_conditional(self, ast):
        assert "condition" in ast
        assert "body" in ast
        assert "else_body" in ast
        condition = ast['condition']
        body = ast['body']
        else_body = ast['else_body']
        assert nodes.istype(condition, nodes.Type.expression)
        assert nodes.istype(body, nodes.Type.statement_body)
        assert (nodes.istype(else_body, nodes.Type.statement_body) or
                else_body is None)
        return ast

    @autotype
    def stmt_while(self, ast):
        assert "condition" in ast
        assert "body" in ast
        condition = ast['condition']
        body = ast['body']
        assert nodes.istype(condition, nodes.Type.expression)
        assert nodes.istype(body, nodes.Type.statement_body)
        return ast

    @autotype
    def stmt_call_method(self, ast):
        assert "assign" in ast
        assert "method" in ast
        assert "arguments" in ast
        assert "builtin" in ast
        assign = ast['assign']
        method = ast['method']
        args = ast['arguments']
        builtin = ast['builtin']

        assert (nodes.istype(assign, nodes.Type.identifier) or
                assign is None)
        assert (nodes.istype(builtin, nodes.Type.builtin_identifier) or
                builtin is None)
        if builtin:
            assert method is None and args is None
            assert ArnoldCBuiltin(builtin['name'])
        else:
            assert nodes.istype(method, nodes.Type.identifier)
            assert isinstance(args, Closure)
            assert all((nodes.istype(arg, nodes.Type.expression)
                        for arg in args))
        return ast

    @autotype
    def stmt_return(self, ast):
        assert "value" in ast
        value = ast['value']
        assert nodes.istype(value, nodes.Type.expression) or value is None
        return ast

    #### EXPRESSIONS ###################################################

    @autotype
    def expression(self, ast):
        assert (nodes.istype(ast, nodes.Type.number) or
                nodes.istype(ast, nodes.Type.identifier))
        return ast

    #### OPERATIONS ####################################################

    @autotype
    def operation_stack(self, ast):
        assert "initial" in ast
        assert "operations" in ast
        initial_term = ast['initial']
        operations = ast['operations']
        assert nodes.istype(initial_term, nodes.Type.expression)
        assert isinstance(operations, Closure)
        assert all((nodes.istype(op, nodes.Type.operation)
                    for op in operations))
        return ast

    @autotype
    def operation(self, ast):
        assert "operator" in ast
        assert "operand" in ast
        operator = ast['operator']
        operand = ast['operand']
        assert (nodes.istype(operator, nodes.Type.math_op) or
                nodes.istype(operator, nodes.Type.logical_op))
        assert nodes.istype(operand, nodes.Type.expression)
        return ast

    @autotype
    def math_op(self, ast):
        ops = {
            "GET UP",
            "GET DOWN",
            "YOU'RE FIRED",
            "HE HAD TO SPLIT",
            "I LET HIM GO"
        }
        assert "op" in ast
        assert isinstance(ast['op'], str)
        assert any((ast['op'] == op for op in ops))
        return ast

    @autotype
    def logical_op(self, ast):
        ops = {
            "YOU ARE NOT YOU YOU ARE ME",
            "LET OFF SOME STEAM BENNET",
            "CONSIDER THAT A DIVORCE",
            "KNOCK KNOCK"
        }
        assert "op" in ast
        assert isinstance(ast['op'], str)
        assert any((ast['op'] == op for op in ops))
        return ast

