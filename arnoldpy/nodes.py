"""Functions for ArnoldC node typing and tree traversal.
"""

import enum

from grako.ast import AST


### NODE TYPING ###############################################################

@enum.unique
class Type(enum.Enum):
    """Enumeration of ArnoldC/Py node types based on the formal grammar.
    """

    error = "WHAT THE FUCK DID I DO WRONG"

    program = 0
    method = 100
    method_main = 101
    method_defined = 102
    nonvoid_flag = 110
    parameter = 120

    identifier = 200
    builtin_identifier = 201
    number = 202
    print_string = 203

    statement = 1000
    statement_body = 1001
    stmt_declaration = 1100
    stmt_assignment =  1101
    stmt_print = 1102
    stmt_conditional = 1103
    stmt_while = 1104
    stmt_call_method = 1105
    stmt_return = 1106

    expression = 2000

    operation_stack = 3000
    operation = 3001
    math_op = 3100
    logical_op = 3200


def addtype(ast, node_type):
    """Append a *node_type* to an AST node. A node may have multiple
    types depending on the production rules, and each rule should append
    its corresponding node type to the node. For example, given the
    production:

        integer = ?/[0-9]+/?;
        string = '"' ?/[^\"]/? '"';
        literal = integer | string;

    The :mod:`grako` semantics class should define:

        class Semantics(object):
            def integer(self, ast):
                # ... any processing here ...

                addtype(ast, "int")
                return ast

            def string(self, ast):
                addtype(ast, "str")
                return ast

            def literal(self, ast):
                addtype(ast, "literal")
                return ast

    The integer node will then have a type of "int" and "literal". (Note
    that is a fictitious example -- :func:`addtype` requires that the
    type be an enumeration of :class:`Type`).
    """

    if node_type not in Type:
        raise ValueError("'{}' is not a valid NodeType".format(node_type))
    try:
        ast._types_.append(node_type)
    except AttributeError:
        setattr(ast, "_types_", [node_type])
        # ast._types_ = [node_type]


def istype(ast, node_type):
    """Check if a node is of *node_type*, appended by :func:`addtype`.
    """

    try:
        return node_type in ast._types_
    except AttributeError:
        return False


def gettypes(ast):
    """Retrieve the list of all types of a *node* appended by
    :func:`addtype`.
    """

    try:
        return ast._types_
    except AttributeError:
        raise TypeError("node '{}' does not have a _type_ attribute"
                        .format(ast.__class__.__name__))



### NODE VISITATION ###########################################################

class NodeVisitor(object):
    """Base class implementing the visitor pattern for AST traversal.

    Each node type set by :func:`addtype` corresponds with a visitation
    and departure method for the :class:`NodeVisitor`. Dispatch is done
    to methods prefixed with ``visit_`` upon entering a node and then
    ``depart_`` when leaving. For example, a method named
    ``visit_stmt_return`` will receive all nodes of
    :attr:`NodeType.stmt_return`

    Each visitation and departure method will receive a *node* and its
    *parent* as arguments.

    If no method exists for a node type, :attr:`default_visit` and
    :attr:`default_depart` is invoked in their place. The default
    implementation does nothing.
    """

    def visit(self, node, parent=None):
        """Dispatch to a node visitation method."""
        # Visit in order of coarse-to-fine.
        for type_enum in gettypes(node)[::-1]:
            name = "visit_" + type_enum.name
            getattr(self, name, self.default_visit)(node, parent)

    def depart(self, node, parent=None):
        """Dispatch to a node departure method."""
        # Depart in reverse order of visit.
        for type_enum in gettypes(node):
            name = "depart_" + type_enum.name
            getattr(self, name, self.default_depart)(node, parent)

    def finalize(self):
        """Completion of node traversal."""

    def default_visit(self, node, parent):
        """Called when no appropriate visitor method exists in this
        class.
        """

    def default_depart(self, node, parent):
        """Called when no appropriate departing method exists in this
        class.
        """

def walk(node, *visitors):
    """Traverse through an AST rooted at *node*, calling the
    :meth:`NodeVisitor.visit` methods of *visitors* when entering each
    node, :meth:`NodeVisitor.depart` when leaving, and
    :meth:`NodeVisitor.finalize` when traversal is complete.
    """

    class _DepartNode(object):
        def __init__(self, node, parent):
            self.node = node
            self.parent = parent

    def _walker(node, parent=None):
        yield node, parent
        for value in node.values():
            if isinstance(value, list):
                for item in value:
                    if isinstance(item, AST):
                        yield from _walker(item, node)
            elif isinstance(value, AST):
                yield from _walker(value, node)
        yield _DepartNode(node, parent)

    for value in _walker(node):
        if isinstance(value, _DepartNode):
            for visitor in visitors:
                visitor.depart(value.node, value.parent)
        else:
            node, parent = value
            for visitor in visitors:
                visitor.visit(node, parent)

    for visitor in visitors:
        visitor.finalize()

