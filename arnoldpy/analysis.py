"""Static semantic analysis of parsed ArnoldPy abstract syntax trees.
These analyzers assert correct ArnoldC semantics that are not parsing or
syntax errors, such as variable redefinition or incorrect method
returns.

Semantic analyzers are implemented as :class:`arnoldpy.nodes.
NodeVisitor` classes, and may be used by walking the parsed AST:

    >>> arnoldpy.nodes.walk(ast, arnoldpy.analysis.MethodReturns())

wherein :exc:`arnoldpy.exceptions.AnalysisError` is raised if any fault
is found.
"""

from arnoldpy.exceptions import AnalysisError

from arnoldpy import nodes
from arnoldpy.symtable import Block


class MethodReturns(nodes.NodeVisitor):
    """Check that all code paths for all methods in a complete ArnoldC
    AST returns correctly. Non-void methods must return with a value and
    void methods must not.

    The tests are simple and does not recognize if a branch condition
    is always true, so a nonvoid method such as this will fail even
    though it is semantically sound:

        func foo()
            if true
                return 0
    """

    # MethodReturns creates a mini representation of the full AST tree,
    # just containing method and conditional blocks, as nested lists,
    # and return statements. Given the pseudocode:
    #
    #     func main()
    #         var a = 1
    #         var b = 0
    #         if a
    #             b = a
    #         print "end"
    #         return
    #
    #     func method(arg1)
    #         if arg1
    #             print "argument is true"
    #         else
    #             return false
    #         return true
    #
    # This tree of lists is formed:
    #
    #     program_block [
    #         method_block [
    #             if_block [
    #                 []
    #             ],
    #             <return>
    #         ],
    #         method_block [
    #             if_block [
    #                 [],
    #                 [<return:has_value>]
    #             ],
    #             <return:has_value>
    #         ]
    #     ]
    #
    # A method's code path is valid if it returns with a value.
    # Returning a value could be either a) an explicit return statement,
    # or b) a conditional path that successfully returns. A conditional
    # only return successfully if it has an else (false) branch and
    # *both* of its branches return.
    #
    # While loops are not considered, but they still exist in the tree.

    ### MINI AST ELEMENTS ##############################################

    class ProgramBlock(list):
        pass

    class MethodBlock(list):
        def __init__(self, name, nonvoid, parseinfo):
            super().__init__()
            self.name = name
            self.nonvoid = nonvoid
            self.parseinfo = parseinfo

    class IfBlock(list):
        pass

    class WhileBlock(list):
        pass

    class Return(object):
        def __init__(self, has_value, parseinfo):
            self.has_value = has_value
            self.parseinfo = parseinfo

    ### OBJECT METHODS #################################################

    def __init__(self):
        self.root = MethodReturns.ProgramBlock()
        self.current_block = self.root
        self.block_stack = []
        self.returns_list = []

    def push_block(self, new_block):
        self.block_stack.append(self.current_block)
        self.current_block = new_block

    def pop_block(self):
        upper_block = self.block_stack.pop()
        upper_block.append(self.current_block)
        self.current_block = upper_block

    def discard_block(self):
        self.current_block = self.block_stack.pop()

    ### NODE VISITOR METHODS ###########################################

    def visit_method(self, node, parent):
        parseinfo = node._parseinfo
        if nodes.istype(node, nodes.Type.method_defined):
            name = node['ident']['name']
            nonvoid = True if node['nonvoid'] else False
        else:
            name = "<main>"
            nonvoid = False
        self.push_block(
            MethodReturns.MethodBlock(name, nonvoid, parseinfo)
        )

    def depart_method(self, node, parent):
        self.returns_list = []
        self.pop_block()

    def visit_stmt_conditional(self, node, parent):
        self.push_block(MethodReturns.IfBlock())

    def depart_stmt_conditional(self, node, parent):
        self.pop_block()

    def visit_stmt_while(self, node, parent):
        self.push_block(MethodReturns.WhileBlock())

    def depart_stmt_while(self, node, parent):
        self.pop_block()

    def visit_statement_body(self, node, parent):
        # Pushing a new block on the bodies `if` will cause it to have
        # have exactly one or two elements (depending if they have an
        # else branch), which are lists.
        if nodes.istype(parent, nodes.Type.stmt_conditional):
            self.push_block(list())

    def depart_statement_body(self, node, parent):
        if nodes.istype(parent, nodes.Type.stmt_conditional):
            self.pop_block()

    def visit_stmt_return(self, node, parent):
        ret = MethodReturns.Return(node['value'], node._parseinfo)
        self.current_block.append(ret)

    def finalize(self):
        self.validate(self.root)

    ### VALIDATORS #####################################################

    def validate(self, program_node):
        """Validate method returns from the mini-tree formed during
        node traversal.
        """

        for method_block in program_node:
            name = method_block.name
            nonvoid = method_block.nonvoid

            if nonvoid:
                self._check_return_values(method_block, True, name)
                if not self._check_code_path(method_block, name):
                    raise AnalysisError(
                        method_block.parseinfo,
                        "non-void '{}': not all code paths return a value"
                            .format(name)
                    )
            else:
                self._check_return_values(method_block, False, name)

    def _check_code_path(self, body, name):
        """Ensure that all code paths in a body return (statements are
        not checked for any return values). Returns a boolean.

        *name* is the method name used to construct error messages.
        """

        for node in body:
            if isinstance(node, MethodReturns.Return):
                return True
            elif isinstance(node, MethodReturns.IfBlock):
                check_body = self._check_code_path(node[0], name)
                try:
                    check_else_body = self._check_code_path(node[1], name)
                except IndexError:
                    # Has no else body -- conditional path cannot be
                    # guaranteed to return.
                    check_else_body = False
                if check_body and check_else_body:
                    return True
        return False

    def _check_return_values(self, body, is_nonvoid, name):
        """Ensure that all return statements in a body returns a value
        (*is_nonvoid* is ``True``) or does not (*is_nonvoid* is
        ``False``) :exc:`arnoldpy.exceptions.AnalysisError` is raised if
        any return statement fails.

        *name* is the method name used to construct error messages.
        """

        for node in body:
            if isinstance(node, MethodReturns.Return):
                if is_nonvoid and not node.has_value:
                    raise AnalysisError(
                        node.parseinfo,
                        "non-void '{}': return without a value".format(name)
                    )
                elif not is_nonvoid and node.has_value:
                    raise AnalysisError(
                        node.parseinfo,
                        "void '{}': cannot return a value".format(name)
                    )
            elif isinstance(node, MethodReturns.IfBlock):
                self._check_return_values(node[0], is_nonvoid, name)
                try:
                    self._check_return_values(node[1], is_nonvoid, name)
                except IndexError:
                    pass
            elif isinstance(node, MethodReturns.WhileBlock):
                self._check_return_values(node, is_nonvoid, name)
        return True


class VariableDeclaration(nodes.NodeVisitor):
    """Check that all variables are declared before usage and that
    no duplicate declarations exist. Within a method, block scope exists
    for conditionals and loops, and a variable in an inner scope cannot
    shadow one from outside. This is illegal:

        func main
            var a = 0
            if true
                var a = 1
                print a
            print a

    But this is legal:

        func main
            if true
                var a = 1
                print a
            var a = 0
            print a

    This is similar to Java's behavior, which ArnoldC is based on
    (but applying this actually fails in the current implementation --
    see ``tests/test_variables.py``). Coincidentally, this also eases
    implementation of scoping rules in Python, which has no block
    scoping semantics on its own.
    """

    def __init__(self):
        self.block = Block()

    def visit_statement_body(self, node, parent):
        # Anything with a statement body -- if, while, method -- is
        # a new block and introduces a new scope.
        self.block.push_scope()

    def depart_statement_body(self, node, parent):
        self.block.pop_scope()

    def visit_method(self, node, parent):
        # Push a scope specifically for methods because its parameters
        # exist in the limbo between the method definition and the
        # statement body. If we don't push a new scope, then they will
        # be added to the global program table, which is a no-no.
        self.block.push_scope()

    def depart_method(self, node, parent):
        self.block.pop_scope()

    def visit_stmt_declaration(self, node, parent):
        ident_name = node['ident']['name']
        try:
            self.block[ident_name] = True
        except KeyError:
            raise AnalysisError(
                node.parseinfo,
                "'{}': duplicate variable".format(ident_name)
            )

    def visit_parameter(self, node, parent):
        ident_name = node['ident']['name']
        try:
            self.block[ident_name] = True
        except KeyError:
            raise AnalysisError(
                node.parseinfo,
                "'{}': duplicate variable".format(ident_name)
            )

    def visit_expression(self, node, parent):
        if nodes.istype(node, nodes.Type.identifier):
            ident_name = node['name']
            try:
                self.block[ident_name]
            except KeyError:
                raise AnalysisError(
                    node.parseinfo,
                    "'{}': variable has not been declared".format(ident_name)
                )
