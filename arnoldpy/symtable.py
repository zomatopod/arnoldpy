import contextlib


class SymbolTable(object):
    def __init__(self, parent=None):
        self.parent = parent
        self._symbols = {}

    def __setitem__(self, key, value):
        if key in self:
            raise KeyError("{}: key already exists".format(key))
        self._symbols[key] = value

    def __getitem__(self, key):
        try:
            return self._symbols[key]
        except KeyError:
            if self.parent is None:
                raise
            return self.parent[key]

    def __contains__(self, key):
        if key in self._symbols:
            return True
        elif self.parent is None:
            return False
        else:
            return key in self.parent

    def __repr__(self):
        return str(self._symbols)


class Block(object):
    def __init__(self):
        self.current_table = SymbolTable()
        self.scopes = []

    def __getitem__(self, key):
        return self.current_table[key]

    def __setitem__(self, key, value):
        self.current_table[key] = value

    def push_scope(self):
        self.scopes.append(self.current_table)
        self.current_table = SymbolTable(parent=self.current_table)

    def pop_scope(self):
        try:
            self.current_table = self.scopes.pop()
        except IndexError:
            raise IndexError("already at highest scope")

    @contextlib.contextmanager
    def scope(self):
        self.push_scope()
        try:
            yield
        finally:
            self.pop_scope()

    def __repr__(self):
        return repr(self.current_table)

