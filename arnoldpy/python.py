"""Facilities to generate Python abstract syntax trees and source code.
"""

import ast as pyast
from functools import wraps

from arnoldpy import nodes
from arnoldpy import parsing


def mangle(name):
    """Mangle Python method names and identifiers.

    All method names are mangled because the reference ArnoldC
    implementation allows variables to have the same identifier as
    methods. Since Python is dynamically typed, we must do this to
    prevent name collisions.
    """

    tmpl = "__{name}"

    if isinstance(name, str):
        name = tmpl.format(name=name)
    elif isinstance(name, pyast.Name):
        name.id = tmpl.format(name=name.id)
    else:
        raise TypeError("could not mangle {}".format(name))
    return name


def lineinfo(func):
    """Extract line and column information from a :mod:`grako` ast (or
    any other object with a :attr:`parseinfo` attribute), and call the
    decorated function with the information as new arguments.

    The rule represented by the semantic function must use named keys
    since :mod:`grako` will not insert parse information otherwise.
    :exc:`TypeError` is raised if the information could not be
    extracted.
    """

    fname = func.__name__

    @wraps(func)
    def wrapper(self, ast):
        if isinstance(ast, list):
            raise TypeError("Got a list of ASTs for '{}' instead of a single "
                            "subscriptable node; did you use named rules in "
                            "the grammar?".format(fname))
        try:
            parseinfo = ast['_parseinfo']
        except AttributeError:
            raise TypeError("Could not find _parseinfo in {} for rule "
                            "'{}'; you use named rules in the "
                            "grammar?".format(ast, fname))
        pos = parseinfo.pos
        lineinfo = parseinfo.buffer.line_info(pos)
        line = lineinfo.line+1
        col = lineinfo.col+1
        return func(self, ast, line, col)
    return wrapper


class Semantics(parsing.Semantics):
    """Produces a Python AST during parsing. Use it as the semantics
    argument for :class:`arnoldpy.parsing.Parser`, where it will it will
    generate the foreign payload on the returned AST.

        >>> from arnoldpy import parsing, python
        >>> parser = parsing.Parser(semantics=python.Semantics())
        >>> ast = parser.parse(arnoldc_source)
        >>> python_ast = ast.foreign_payload

    The Python AST may then be compiled into a code object with
    :func:`compile`.
    """

    ############################################
    ## SEMANTIC METHODS ########################
    ############################################

    #### STRUCTURE #####################################################

    def program(self, ast):
        methods = ast['methods']
        assert isinstance(methods, list)
        assert all((isinstance(meth, pyast.FunctionDef) for meth in methods))

        return pyast.Module([meth for meth in methods])

    def method(self, ast):
        method = ast
        assert isinstance(method, pyast.FunctionDef)

    @lineinfo
    def method_main(self, ast, line, col):
        body = ast['body']
        assert isinstance(body, list)
        assert body
        assert all((isinstance(stmt, pyast.stmt) for stmt in body))

        return pyast.FunctionDef(
            name=mangle("main"),
            args=pyast.arguments([], None, [], [], None, []),
            body=body,
            decorator_list=[],
            returns=None,
            lineno=line,
            col_offset=col
        )

    @lineinfo
    def method_defined(self, ast, line, col):
        ident = ast['ident']
        parameters = ast['parameters'] or []
        body = ast['body']
        nonvoid = ast['nonvoid']
        assert isinstance(ident, pyast.Name)
        assert isinstance(parameters, list)
        assert isinstance(body, list)
        assert body
        assert all((isinstance(param, pyast.arg) for param in parameters))
        assert all((isinstance(stmt, pyast.stmt) for stmt in body))

        return pyast.FunctionDef(
            name=mangle(ident.id),
            args=pyast.arguments(
                args=parameters,
                vararg=None,
                kwonlyargs=[],
                kw_defaults=[],
                kwarg=None,
                defaults=[]
            ),
            body=body,
            decorator_list=[],
            returns=None,
            nonvoid=nonvoid,
            lineno=line,
            col_offset=col
        )

    def nonvoid_flag(self, ast):
        pass

    @lineinfo
    def parameter(self, ast, line, col):
        ident = ast['ident']
        assert isinstance(ident, pyast.Name)
        return pyast.arg(
            arg=ident.id,
            annotation=None,
            lineno=line,
            col=col
        )

    #### ATOMS #########################################################

    @lineinfo
    def identifier(self, ast, line, col):
        assert isinstance(ast['name'], str)
        return pyast.Name(
            ast['name'],
            pyast.Load(),
            lineno=line,
            col_offset=col
        )

    @lineinfo
    def builtin_identifier(self, ast, line, col):
        name = ast['name']
        assert isinstance(name, str)
        return pyast.Name(
            name,
            pyast.Load(),
            lineno=line,
            col_offset=col
        )

    @lineinfo
    def number(self, ast, line, col):
        assert isinstance(ast['num'], str)
        return pyast.Num(
            int(ast['num']),
            lineno=line,
            col_offset=col
        )

    @lineinfo
    def print_string(self, ast, line, col):
        assert isinstance(ast['string'], str)
        return pyast.Str(
            ast['string'],
            lineno=line,
            col_offset=col
        )

    #### STATEMENTS ####################################################

    @lineinfo
    def statement_body(self, ast, line, col):
        # Python semantic rules prohibit statement bodies from being
        # empty and requires a no-op -- the ``pass`` statement -- if no
        # operation is desired.
        statements = ast['statements']
        assert isinstance(statements, list)
        assert all((isinstance(stmt, pyast.stmt) for stmt in statements))
        if not statements:
            statements.append(pyast.Pass(lineno=line, col_offset=col))
        return statements

    def statement(self, ast):
        assert isinstance(ast, pyast.stmt)

    @lineinfo
    def stmt_declaration(self, ast, line, col):
        ident = ast['ident']
        value = ast['value']
        assert isinstance(ident, pyast.Name)
        assert isinstance(value, pyast.expr)

        ident.ctx = pyast.Store()
        return pyast.Assign(
            [ident],
            value,
            lineno=line,
            col_offset=col
        )

    @lineinfo
    def stmt_assignment(self, ast, line, col):
        ident, expr = ast['ident'], ast['operation']
        assert isinstance(ident, pyast.Name)
        assert isinstance(expr, pyast.expr)

        ident.ctx = pyast.Store()
        return pyast.Assign(
            [ident],
            expr,
            lineno=line,
            col_offset=col
        )

    @lineinfo
    def stmt_print(self, ast, line, col):
        value = ast['value']
        assert isinstance(value, pyast.expr)

        return self.make_func_call(
            pyast.Name("print", pyast.Load(), lineno=line, col_offset=col),
            [value],
            None,
            line,
            col
        )

    @lineinfo
    def stmt_conditional(self, ast, line, col):
        condition = ast['condition']
        body = ast['body']
        else_body = ast['else_body'] or []
        # Note that unlike the true statement body, the false body CAN
        # be empty without a no-op. Python considers such a conditional
        # as simply lacking an else branch.
        assert isinstance(condition, pyast.expr)
        assert isinstance(body, list)
        assert isinstance(else_body, list)
        assert body
        assert all((isinstance(stmt, pyast.stmt) for stmt in body))
        assert all((isinstance(stmt, pyast.stmt) for stmt in else_body))

        return pyast.If(
            condition,
            body,
            else_body,
            lineno=line,
            col_offset=col
        )

    @lineinfo
    def stmt_while(self, ast, line, col):
        condition = ast['condition']
        body = ast['body']
        assert isinstance(condition, pyast.expr)
        assert isinstance(body, list)
        assert body
        assert all((isinstance(stmt, pyast.stmt) for stmt in body))

        return pyast.While(
            condition,
            body,
            [],
            lineno=line,
            col_offset=col
        )

    @lineinfo
    def stmt_call_method(self, ast, line, col):
        assign = ast['assign']
        method = ast['method']
        args = ast['arguments']
        builtin = ast['builtin']
        assert isinstance(assign, pyast.Name) or assign is None
        assert isinstance(builtin, pyast.Name) or builtin is None
        if builtin:
            assert method is None and args is None
            return self.make_builtin_func_call(builtin, [], assign, line, col)
        else:
            assert isinstance(method, pyast.Name)
            assert isinstance(args, list)
            assert all((isinstance(arg, pyast.expr) for arg in args))
            return self.make_func_call(mangle(method), args, assign, line, col)

    @lineinfo
    def stmt_return(self, ast, line, col):
        value = ast['value']
        assert isinstance(value, pyast.expr) or value is None
        return pyast.Return(
            value,
            lineno=line,
            col_offset=col
        )

    #### EXPRESSIONS ###################################################

    def expression(self, ast):
        assert (isinstance(ast, pyast.Num) or
                isinstance(ast, pyast.Name))

    #### OPERATIONS ####################################################

    def operation_stack(self, ast):
        initial_term, operations = ast['initial'], ast['operations']
        assert isinstance(initial_term, pyast.expr)
        assert isinstance(operations, list)
        assert all((isinstance(expr, pyast.expr) for expr in operations))

        left_term = initial_term
        for operation in operations:
            # BinOp and CompOp have .left attributes for the first operand.
            if getattr(operation, "left", None) is not None:
                operation.left = left_term
            # BoolOp has a list of values instead.
            else:
                operation.values[0] = left_term

            # Convert to int for boolean operations, as per ArnoldC
            # semantics.
            if getattr(operation, "convert_to_boolint", False):
                left_term = self.make_boolint_conversion(operation)
            else:
                left_term = operation

        return left_term

    @lineinfo
    def operation(self, ast, line, col):
        operator, operand = ast['operator'], ast['operand']
        assert (isinstance(operator, pyast.operator) or
                isinstance(operator, pyast.cmpop) or
                isinstance(operator, pyast.boolop))
        assert isinstance(operand, pyast.expr)

        # 0 = left operand temp value. It will be properly set in the
        # `expression` rule.
        if isinstance(operator, pyast.operator):
            node = pyast.BinOp(
                0,
                operator,
                operand,
                lineno=line,
                col_offset=col
            )
        elif isinstance(operator, pyast.cmpop):
            node = pyast.Compare(
                0,
                [operator],
                [operand],
                lineno=line,
                col_offset=col
            )
            node.convert_to_boolint = True
        else:
            node = pyast.BoolOp(
                operator,
                [0, operand],
                lineno=line,
                col_offset=col
            )
            node.convert_to_boolint = True

        return node

    def math_op(self, ast):
        ops = {
            # pyast.operator operators
            "GET UP": pyast.Add,
            "GET DOWN": pyast.Sub,
            "YOU'RE FIRED": pyast.Mult,
            "HE HAD TO SPLIT": pyast.FloorDiv, # ArnoldC has no floats!
            "I LET HIM GO": pyast.Mod
        }
        op = ast['op']
        assert isinstance(op, str)
        assert op in ops

        return ops[op]()

    def logical_op(self, ast):
        ops = {
            # pyast.cmpop operators
            "YOU ARE NOT YOU YOU ARE ME": pyast.Eq,
            "LET OFF SOME STEAM BENNET": pyast.Gt,
            # pyast.boolop operators
            "CONSIDER THAT A DIVORCE": pyast.Or,
            "KNOCK KNOCK": pyast.And
        }
        op = ast['op']
        assert isinstance(op, str)
        assert op in ops

        return ops[op]()

    ############################################
    ## UTILITIES ###############################
    ############################################

    def make_boolint_conversion(self, ast):
        """Wrap and return an expression that returns a boolean 0 or 1.
        This is done simply by creating an AST expression for
        ``int(bool(ast))``.
        """

        line, col = ast.lineno, ast.col_offset
        bool_call = pyast.Call(
            pyast.Name('bool', pyast.Load(), lineno=line, col_offset=col),
            [ast],
            [],
            None,
            None,
            lineno=line,
            col_offset=col
        )
        int_call = pyast.Call(
            pyast.Name('int', pyast.Load(), lineno=line, col_offset=col),
            [bool_call],
            [],
            None,
            None,
            lineno=line,
            col_offset=col
        )
        return int_call

    def make_func_call(self, func, args=None, ident=None, line=0, col=0):
        """Return a statement node to a call of a function with the name
        *func* (:class:`ast.Name` or string) and *args* positional
        arguments (list), with an optional assignment of the result to
        the variable represented by *ident* (:class:`ast.Name` or
        bare string).

        With *ident*, the returned node is :class:`ast.Assign`, or
        :class:`Expr` otherwise.
        """

        if isinstance(func, str):
            func = pyast.Name(func, pyast.Load(), lineno=0, col_offset=col)
        call = pyast.Call(
            func,
            args or [],
            [],
            None,
            None,
            lineno=line,
            col_offset=col
        )
        if ident:
            ident.ctx = pyast.Store()
            return pyast.Assign(
                [ident],
                call,
                lineno=line,
                col_offset=col
            )
        else:
            return pyast.Expr(
                call,
                lineno=line,
                col_offset=col
            )

    def make_builtin_func_call(self, func, args=None, ident=None,
                               line=0, col=0):
        """Return a function call statement, much like :func:
        `make_func_call`, except the target is a pre-defined, builtin
        function.
        """

        enum = parsing.ArnoldCBuiltin(func.id)
        if enum == parsing.ArnoldCBuiltin.read_input:
            read_call = pyast.Call(
                pyast.Name("input", pyast.Load(), lineno=line, col_offset=col),
                [],
                [],
                None,
                None,
                lineno=line,
                col_offset=col
            )
            return self.make_func_call("int", [read_call], ident, line, col)


def unparse(ast, exec_main=False):
    """Convenience function for :meth:`Unparser.unparse` -- see that
    method's documentation for more details.
    """

    unp = Unparser()
    return unp.unparse(ast, exec_main)


class Unparser(nodes.NodeVisitor):
    """Convert an AST generated by :class:`arnoldpy.parsing.Parser` into
    an equivalent Python plaintext source.
    """

    INDENT_SPACES = 4

    def __init__(self):
        self.buffer = None
        self.line = None
        self.indent_level = 0

        # Prevent external node traversal.
        def prohibit(n, p):
            raise AttributeError("cannot visit or depart nodes in {} "
                                 "from an external source"
                                 .format(self.__class__.__name__))
        self.visit = prohibit
        self.depart = prohibit
        self._visit = super().visit
        self._depart = super().depart

    def unparse(self, ast, exec_main=False):
        """Translates *ast* into Python source, returned as a string.
        If *exec_main* is ``True``, a canonical ``if __name__ ==
        "__main__":`` execution guard is appended to allow the script to
        be directly run.
        """

        self.buffer = []
        self.line = []
        self.indent_level = 0
        self.dispatch(ast)
        if exec_main:
            self.buffer.extend([
                "if __name__ == '__main__':\n",
                "{}{}()".format(" "*self.INDENT_SPACES, mangle("main"))
            ])
        return "".join(self.buffer)

    def write(self, text):
        self.line.append(text)

    def newline(self):
        self.buffer.append(" "*self.INDENT_SPACES*self.indent_level)
        self.buffer.append("".join(self.line))
        self.buffer.append("\n")
        self.line = []

    def dispatch(self, node, parent=None):
        """Dispatches visitation and departure nodes. Used internally
        by the node visitor methods to allow them to self-walk the
        tree in the order required.
        """

        self._visit(node, None)
        self._depart(node, None)

    ### NODE VISITOR METHODS ###########################################

    def visit_program(self, node, parent):
        for meth in node['methods']:
            self.dispatch(meth)

    def visit_statement_body(self, node, parent):
        self.indent_level += 1
        if node['statements']:
            for stmt in node['statements']:
                self.dispatch(stmt)
        else:
            self.write("pass")
            self.newline()

    def depart_statement_body(self, node, parent):
        self.indent_level -= 1

    def visit_method(self, node, parent):
        self.write("def ")
        try:
            # Directly write the ident node instead of dispatching to
            # the ident visitor since we need to python.mangle the name.
            self.write(mangle(node['ident']['name']))
            self.write("(")
        except KeyError:
            self.write(mangle("main"))
            self.write("(")
        else:
            if node['parameters'] is not None:
                last = len(node['parameters']) - 1
                for i, param in enumerate(node['parameters']):
                    self.dispatch(param['ident'])
                    if i < last:
                        self.write(", ")
        self.write("):")
        self.newline()
        self.dispatch(node['body'])

    def visit_identifier(self, node, parent):
        self.write(node['name'])

    def visit_number(self, node, parent):
        self.write(node['num'])

    def visit_print_string(self, node, parent):
        self.write("\"{}\"".format(node['string']))

    def visit_stmt_declaration(self, node, parent):
        self.dispatch(node['ident'])
        self.write(" = ")
        self.dispatch(node['value'])
        self.newline()

    def visit_stmt_call_method(self, node, parent):
        if node['assign'] is not None:
            self.dispatch(node['assign'])
            self.write(" = ")
        if node['builtin'] is not None:
            self.write_builtin_func(node['builtin'])
        else:
            # Methd name mangling as in visit_method.
            self.write(mangle(node['method']['name']))
        self.write("(")
        if node['arguments'] is not None:
            last = len(node['arguments']) - 1
            for i, arg in enumerate(node['arguments']):
                self.dispatch(arg, node)
                if i < last:
                    self.write(", ")
        self.write(")")
        self.newline()

    def visit_stmt_while(self, node, parent):
        self.write("while ")
        self.dispatch(node['condition'])
        self.write(":")
        self.newline()
        self.dispatch(node['body'])

    def visit_stmt_conditional(self, node, parent):
        self.write("if ")
        self.dispatch(node['condition'])
        self.write(":")
        self.newline()
        self.dispatch(node['body'])
        if node['else_body'] is not None:
            self.write("else:")
            self.newline()
            self.dispatch(node['else_body'])

    def visit_stmt_assignment(self, node, parent):
        self.dispatch(node['ident'], node)
        self.write(" = ")
        self.dispatch(node['operation'], node)
        self.newline()

    def visit_stmt_print(self, node, parent):
        self.write("print(")
        self.dispatch(node['value'])
        self.write(")")
        self.newline()

    def visit_stmt_return(self, node, parent):
        self.write("return ")
        if node['value'] is not None:
            self.dispatch(node['value'])
        self.newline()

    def visit_operation_stack(self, node, parent):
        # Collect operations stack into recursively grouped terms.
        if node['operations']:
            left_term = node['initial']
            for op_node in node['operations']:
                group = _BinaryOp(
                    self,
                    left=left_term,
                    operator=op_node['operator'],
                    right=op_node['operand']
                )
                left_term = group
            left_term.write()
        # Single term assignment with no binary operations.
        else:
            self.dispatch(node['initial'])

    def visit_operation(self, node, parent):
        self.dispatch(node['operator'])
        self.dispatch(node['operand'])

    math_ops = {
        "GET UP": "+",
        "GET DOWN": "-",
        "YOU'RE FIRED": "*",
        "HE HAD TO SPLIT": "//",
        "I LET HIM GO": "%"
    }
    def visit_math_op(self, node, parent):
        self.write(" {} ".format(self.math_ops[node['op']]))

    logical_ops = {
        "YOU ARE NOT YOU YOU ARE ME": "==",
        "LET OFF SOME STEAM BENNET": ">",
        "CONSIDER THAT A DIVORCE": "or",
        "KNOCK KNOCK": "and"
    }
    def visit_logical_op(self, node, parent):
        self.write(" {} ".format(self.logical_ops[node['op']]))

    #### UTILITY #######################################################

    def write_builtin_func(self, builtin_id_node):
        """Write a predefined Python function depending on the enum
        of *builtin_id_node*.
        """

        enum = parsing.ArnoldCBuiltin(builtin_id_node['name'])
        if enum == parsing.ArnoldCBuiltin.read_input:
            self.write("input")


class _BinaryOp(object):
    # Utility class that holds a binary math/logical operation and
    # prints them as parenthesized terms.

    def __init__(self, unparser, left, operator, right):
        self.left = left
        self.operator = operator
        self.right = right
        self.unparser = unparser

    def write(self):
        int_coerce = nodes.istype(self.operator, nodes.Type.logical_op)
        if int_coerce:
            # Boolean results need to be coerced as 0 or 1.
            self.unparser.write("int(bool(")
        else:
            self.unparser.write("(")
        try:
            self.left.write()
        except TypeError:
            self.unparser.dispatch(self.left)
        self.unparser.dispatch(self.operator)
        self.unparser.dispatch(self.right)
        if int_coerce:
            self.unparser.write("))")
        else:
            self.unparser.write(")")