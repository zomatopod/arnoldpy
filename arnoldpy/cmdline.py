"""ArnoldPy command-line application and entry point.
"""

import pathlib
import sys

import click

import arnoldpy
from arnoldpy import parsing
from arnoldpy import python
from arnoldpy.exceptions import ParseError


@click.command()
@click.argument(
    'source_file',
    type=click.File('r', encoding='utf-8'))
@click.option(
    '--transpile',
    '-t',
    is_flag=True,
    help="Transcompile the ArnoldC input into an equivalent Python source.")
@click.option(
    '--output',
    '-o',
    type=click.File('w', encoding='utf-8'),
    help="The output file path when using --transpile.")
@click.option(
    '--run',
    '-r',
    is_flag=True,
    help=("When used with --transpile, also run the script after compiling "
          "the Python source."))
@click.version_option(
    prog_name="ArnoldPy",
    version=arnoldpy.__version__)
def main(source_file, run, transpile, output):
    """A not-entirely-accurate Python implementation of ArnoldC.

    System stdin/stdout may be used by passing a dash ("-") in lieu of a
    filename.
    """

    source_path = pathlib.Path(source_file.name)

    ast = parse(source_file, source_path)
    source_file.close()

    if not transpile:
        execute(ast, source_path)
    else:
        if not output:
            output = open(source_path.stem + ".py", "w", encoding="utf-8")
        transcompile(ast, output)
        output.close()
        if run:
            execute(ast, source_path)


def parse(source_file, source_path):
    source = source_file.read()
    parser = parsing.Parser(python.Semantics())
    try:
        ast = parser.parse(source, filename=str(source_path))
    except ParseError as err:
        exit_fail(str(err))
    return ast


def execute(ast, source_path):
    code = compile(ast.foreign_payload, str(source_path), 'exec')
    namespace = {}
    exec(code, namespace)
    try:
        namespace[python.mangle("main")]()
    except KeyError:
        # No main()
        pass


def transcompile(ast, output_file):
    pysource = python.unparse(ast, exec_main=True)
    output_file.write(pysource)


def exit_fail(message):
    try:
        import colorama
        colorama.init()
        click.echo(colorama.Fore.RED + colorama.Style.BRIGHT + message,
                   file=sys.stderr)
    except ImportError:
        click.echo(message, file=sys.stderr)
    exit(1)
