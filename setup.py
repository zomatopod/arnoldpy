import pathlib
from setuptools import setup, find_packages


ROOT_DIR = pathlib.Path(__file__).parent

def read(fn):
    with (ROOT_DIR / fn).open('r') as file_:
        return file_.read()

def version(pkg):
    with (ROOT_DIR / pkg / "VERSION").open('r') as ver_file:
        return ver_file.read().strip()

setup(
    name="ArnoldPy",
    version=version("arnoldpy"),
    author="Lindy Meas",
    description=(
        "What is the best in life? A not-entirely-accurate implementation of "
        "ArnoldC in Python."
    ),
    long_description=read("README.rst"),
    license="MIT",
    install_requires=[
        "grako>=3.0.1",
        "click>=2.2"
    ],
    entry_points={
        'console_scripts': 'arnoldpy = arnoldpy:run_cmdline'
    },
    packages=find_packages(),
    include_package_data = True
)
