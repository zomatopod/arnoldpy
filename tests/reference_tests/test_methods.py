"""Method tests. These are adapted from the ArnoldC reference
implementation's ``MethodTest.scala``.
"""

import pytest

from arnoldpy.exceptions import ParseError


def test_eval_method_not_main_vol1(execute):
    code = (
        "LISTEN TO ME VERY CAREFULLY mymethod\n"
        "HASTA LA VISTA, BABY\n"
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND \"Hello\"\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "Hello\n"


def test_eval_method_not_main_vol2(execute):
    code = (
        "LISTEN TO ME VERY CAREFULLY mymethod\n"
        "HASTA LA VISTA, BABY\n"
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND \"Hello\"\n"
        "YOU HAVE BEEN TERMINATED"
    )
    assert execute(code) == "Hello\n"


def test_eval_method_not_main_vol3(execute):
    code = (
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND \"Hello\"\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY mymethod\n"
        "HASTA LA VISTA, BABY\n"
    )
    assert execute(code) == "Hello\n"


def test_eval_method_not_main_vol4(execute):
    code = (
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND \"Hello\"\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY mymethod\n"
        "HASTA LA VISTA, BABY"
    )
    assert execute(code) == "Hello\n"


def test_plain_method_call(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW printHello\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY printHello\n"
        "TALK TO THE HAND \"Hello\"\n"
        "HASTA LA VISTA, BABY"
    )
    assert execute(code) == "Hello\n"


def test_method_call_with_argument(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE argument\n"
        "YOU SET US UP 123\n"
        "DO IT NOW printInteger argument\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY printInteger\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value\n"
        "TALK TO THE HAND value\n"
        "HASTA LA VISTA, BABY"
    )
    assert execute(code) == "123\n"


def test_multiple_method_calls(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW printHello\n"
        "DO IT NOW printCheers\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY printHello\n"
        "TALK TO THE HAND \"Hello\"\n"
        "HASTA LA VISTA, BABY\n"
        "LISTEN TO ME VERY CAREFULLY printCheers\n"
        "TALK TO THE HAND \"Cheers\"\n"
        "HASTA LA VISTA, BABY"
    )
    assert execute(code) == "Hello\nCheers\n"


def test_method_call_inside_method_call(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW printHello\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY printHello\n"
        "TALK TO THE HAND \"Hello\"\n"
        "DO IT NOW printCheers\n"
        "DO IT NOW printHejsan\n"
        "HASTA LA VISTA, BABY\n"
        "LISTEN TO ME VERY CAREFULLY printCheers\n"
        "TALK TO THE HAND \"Cheers\"\n"
        "HASTA LA VISTA, BABY\n"
        "LISTEN TO ME VERY CAREFULLY printHejsan\n"
        "TALK TO THE HAND \"Hejsan\"\n"
        "HASTA LA VISTA, BABY"
    )
    assert execute(code) == "Hello\nCheers\nHejsan\n"


def test_return_in_void_method(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW method\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY method\n"
        "I'LL BE BACK\n"
        "HASTA LA VISTA, BABY\n"

    )
    assert execute(code) == ""


def test_multiple_return_in_void_method_vol1(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW printboolean @NO PROBLEMO\n"
        "DO IT NOW printboolean @I LIED\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY printboolean\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value\n"
        "BECAUSE I'M GOING TO SAY PLEASE value\n"
        "TALK TO THE HAND \"true\"\n"
        "I'LL BE BACK\n"
        "BULLSHIT\n"
        "TALK TO THE HAND \"false\"\n"
        "I'LL BE BACK\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "HASTA LA VISTA, BABY\n"
    )
    assert execute(code) == "true\nfalse\n"


def test_multiple_return_in_void_method_vol2(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW printboolean @NO PROBLEMO\n"
        "DO IT NOW printboolean @I LIED\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY printboolean\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value\n"
        "BECAUSE I'M GOING TO SAY PLEASE value\n"
        "TALK TO THE HAND \"true\"\n"
        "BULLSHIT\n"
        "TALK TO THE HAND \"false\"\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "HASTA LA VISTA, BABY\n"
    )
    assert execute(code) == "true\nfalse\n"


def test_multiple_return_in_void_method_vol3(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW printboolean @NO PROBLEMO\n"
        "DO IT NOW printboolean @I LIED\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY printboolean\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value\n"
        "BECAUSE I'M GOING TO SAY PLEASE value\n"
        "TALK TO THE HAND \"true\"\n"
        "BULLSHIT\n"
        "TALK TO THE HAND \"false\"\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "I'LL BE BACK\n"
        "I'LL BE BACK\n"
        "HASTA LA VISTA, BABY\n"
    )
    assert execute(code) == "true\nfalse\n"



def test_multiple_return_in_void_method_with_unreachable_code(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW method\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY method\n"
        "TALK TO THE HAND \"reached codeblock\"\n"
        "I'LL BE BACK\n"
        "TALK TO THE HAND \"unreached codeblock\"\n"
        "HASTA LA VISTA, BABY\n"
    )
    assert execute(code) == "reached codeblock\n"


def test_void_method_returning_from_branch_statement(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW reverse @NO PROBLEMO\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY reverse\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value\n"
        "BECAUSE I'M GOING TO SAY PLEASE value\n"
        "TALK TO THE HAND \"evaluated\"\n"
        "I'LL BE BACK\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "TALK TO THE HAND \"not evaluated\"\n"
        "I'LL BE BACK\n"
        "HASTA LA VISTA, BABY\n"
    )
    assert execute(code) == "evaluated\n"


def test_nonvoid_method_returning_from_branch_statement(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW reverse @NO PROBLEMO\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY reverse\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value\n"
        "GIVE THESE PEOPLE AIR\n"
        "BECAUSE I'M GOING TO SAY PLEASE value\n"
        "TALK TO THE HAND \"evaluated\"\n"
        "I'LL BE BACK 0\n"
        "TALK TO THE HAND \"evaluated\"\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "TALK TO THE HAND \"not evaluated\"\n"
        "I'LL BE BACK 0\n"
        "HASTA LA VISTA, BABY\n"
    )
    assert execute(code) == "evaluated\n"


def test_variable_assignment_from_method_call(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP 0\n"
        "GET YOUR ASS TO MARS result\n"
        "DO IT NOW square 7\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY square\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value\n"
        "GIVE THESE PEOPLE AIR\n"
        "GET TO THE CHOPPER value\n"
        "HERE IS MY INVITATION value\n"
        "YOU'RE FIRED value\n"
        "ENOUGH TALK\n"
        "I'LL BE BACK value\n"
        "HASTA LA VISTA, BABY\n"
    )
    assert execute(code) == "49\n"


def test_dectect_unclosed_main(execute):
    code = (
        "IT'S SHOWTIME\n"
        "LISTEN TO ME VERY CAREFULLY printHello\n"
        "TALK TO THE HAND \"Hello\"\n"
    )
    with pytest.raises(ParseError):
        execute(code)


def test_detect_unclosed_method(execute):
    code = (
        "IT'S SHOWTIME\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY printHello\n"
        "TALK TO THE HAND \"Hello\"\n"
    )
    with pytest.raises(ParseError):
        execute(code)


def test_detect_calls_to_undeclared_methods(execute):
    # This test is slightly divergent from standard ArnolC.
    #
    # In reference ArnoldC, this is a parsing error, but it is a
    # runtime error in ArnoldPy.

    code = (
       "IT'S SHOWTIME\n"
        "DO IT NOW noSuchMethod\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    with pytest.raises(NameError):
        execute(code)


def test_detect_void_method_returning_a_value(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW method\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY method\n"
        "I'LL BE BACK 0\n"
        "HASTA LA VISTA, BABY\n"
    )
    with pytest.raises(ParseError):
        execute(code)


def test_detect_nonvoid_method_returning_without_a_value(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW method 0\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY method\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value\n"
        "GIVE THESE PEOPLE AIR\n"
        "I'LL BE BACK\n"
        "HASTA LA VISTA, BABY\n"
    )
    with pytest.raises(ParseError):
        execute(code)
