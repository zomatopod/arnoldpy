"""Branching (looping and conditional) tests. These are adapted from the
ArnoldC reference implementation's ``BranchStatementTest.scala``.
"""


def test_simple_if_statements_vol1(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE vartrue\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "BECAUSE I'M GOING TO SAY PLEASE vartrue\n"
        "TALK TO THE HAND \"this branch should be reached\"\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "this branch should be reached\n"


def test_simple_if_statements_vol2(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE vartrue\n"
        "YOU SET US UP @I LIED\n"
        "BECAUSE I'M GOING TO SAY PLEASE vartrue\n"
        "TALK TO THE HAND \"this branch should not be reached\"\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == ""


def test_simple_if_else_statements_vol1(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE vartrue\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "BECAUSE I'M GOING TO SAY PLEASE vartrue\n"
        "TALK TO THE HAND \"this branch should be reached\"\n"
        "BULLSHIT\n"
        "TALK TO THE HAND \"this branch should not be reached\"\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "this branch should be reached\n"


def test_simple_if_else_statements_vol2(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE varfalse\n"
        "YOU SET US UP @I LIED\n"
        "BECAUSE I'M GOING TO SAY PLEASE varfalse\n"
        "TALK TO THE HAND \"this branch should not be reached\"\n"
        "BULLSHIT\n"
        "TALK TO THE HAND \"this branch should be reached\"\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "this branch should be reached\n"


def test_assign_variables_in_if_statements(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP 0\n"
        "HEY CHRISTMAS TREE vartrue\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "BECAUSE I'M GOING TO SAY PLEASE vartrue\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION 3\n"
        "ENOUGH TALK\n"
        "YOU HAVE NO RESPECT FOR LOGIC\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "3\n"


def test_stube_while_statements_vol1(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE varfalse\n"
        "YOU SET US UP @I LIED\n"
        "STICK AROUND varfalse\n"
        "CHILL\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == ""


def test_stube_while_statements_vol2(execute):
    code = (
        "IT'S SHOWTIME\n"
        "STICK AROUND @I LIED\n"
        "CHILL\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == ""


def test_one_loop_iteration(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE varfalse\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "STICK AROUND varfalse\n"
        "GET TO THE CHOPPER varfalse\n"
        "HERE IS MY INVITATION @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND \"while statement printed once\"\n"
        "CHILL\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "while statement printed once\n"


def test_conditional_loop_iteration(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE isLessThan10\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "HEY CHRISTMAS TREE n\n"
        "YOU SET US UP 0\n"
        "STICK AROUND isLessThan10\n"
        "GET TO THE CHOPPER n\n"
        "HERE IS MY INVITATION n\n"
        "GET UP 1\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND n\n"
        "GET TO THE CHOPPER isLessThan10\n"
        "HERE IS MY INVITATION 10\n"
        "LET OFF SOME STEAM BENNET n\n"
        "ENOUGH TALK\n"
        "CHILL\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n"
