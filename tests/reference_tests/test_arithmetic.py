"""Arithmetic tests. These are adapted from the ArnoldC reference
implementation's ``ArithmeticTest.scala``.
"""

import pytest

from arnoldpy.exceptions import ParseError


def test_declare_variable(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP 123\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == ""


def test_print_integer(execute):
    code = (
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND 123\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "123\n"


def test_print_negative_integer(execute):
    code = (
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND -111\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "-111\n"


def test_print_boolean(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE varfalse\n"
        "YOU SET US UP @I LIED\n"
        "TALK TO THE HAND varfalse\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_print_string(execute):
    code = (
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND \"this should be printed\"\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "this should be printed\n"


def test_print_exotic_string(execute):
    code = (
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND \"!!! ??? äöäöäöä@0123=+-,.\"\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "!!! ??? äöäöäöä@0123=+-,.\n"


def test_declare_and_print_integer(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE A\n"
        "YOU SET US UP 999\n"
        "HEY CHRISTMAS TREE B\n"
        "YOU SET US UP 555\n"
        "TALK TO THE HAND A\n"
        "TALK TO THE HAND B\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "999\n555\n"


def test_declare_and_print_negative_integer(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE a\n"
        "YOU SET US UP -999\n"
        "HEY CHRISTMAS TREE b\n"
        "YOU SET US UP -555\n"
        "TALK TO THE HAND a\n"
        "TALK TO THE HAND b\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "-999\n-555\n"


def test_assign_variable(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION 123\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "123\n"


def test_assign_multiple_variables(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP 22\n"
        "HEY CHRISTMAS TREE var2\n"
        "YOU SET US UP 27\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION 123\n"
        "ENOUGH TALK\n"
        "GET TO THE CHOPPER var2\n"
        "HERE IS MY INVITATION 707\n"
        "ENOUGH TALK\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION var2\n"
        "GET UP var\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "830\n"


def test_increment_integer_and_print(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "GET UP 44\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "66\n"


def test_decrement_integer_and_print(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "GET DOWN 44\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "-22\n"


def test_decrement_negative_value(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "GET DOWN -44\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "66\n"



def test_increment_negative_value(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "GET UP -44\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "-22\n"


def test_multiply_variables(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "YOU'RE FIRED 13\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "286\n"


def test_multiply_with_different_signs(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "YOU'RE FIRED -13\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "-286\n"


def test_multiply_variables_with_zero(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "YOU'RE FIRED 0\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_multiply_assigned_variables(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 7\n"
        "HEY CHRISTMAS TREE VAR2\n"
        "YOU SET US UP 4\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "YOU'RE FIRED VAR2\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "28\n"


def test_divide_variables(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 100\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "HE HAD TO SPLIT 4\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "25\n"


def test_divide_with_different_signs(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 99\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "HE HAD TO SPLIT -33\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "-3\n"


def test_divide_with_one(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "HE HAD TO SPLIT 1\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "22\n"


def test_divide_assigned_variables(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 9\n"
        "HEY CHRISTMAS TREE VAR2\n"
        "YOU SET US UP 4\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION VAR\n"
        "HE HAD TO SPLIT VAR2\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "2\n"


def test_calc_modulo_variables_vol1(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP 1\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION var\n"
        "I LET HIM GO 2\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_calc_modulo_variables_vol2(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP 2\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION var\n"
        "I LET HIM GO 2\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_combined_arithmatic_vol1(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION 11\n"
        "GET DOWN 43\n"
        "GET UP 54\n"
        "GET UP 44\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "66\n"


def test_combined_arithmatic_vol2(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION 11\n"
        "GET DOWN 55\n"
        "GET UP 11\n"
        "GET UP 22\n"
        "GET UP 23\n"
        "GET DOWN 0\n"
        "GET UP 0\n"
        "GET UP 1\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "13\n"


def test_combined_arithmatic_vol3(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "GET TO THE CHOPPER VAR\n"
        "HERE IS MY INVITATION 11\n"
        "GET DOWN 22\n"
        "HE HAD TO SPLIT -11\n"
        "YOU'RE FIRED 23\n"
        "GET UP 23\n"
        "GET DOWN 22\n"
        "HE HAD TO SPLIT 2\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND VAR\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "12\n"


def test_detect_duplicate_variable_declarations(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "HEY CHRISTMAS TREE VAR\n"
        "YOU SET US UP 22\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    with pytest.raises(ParseError):
        execute(code)


def test_faulty_variable_names(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE 1VAR\n"
        "YOU SET US UP 123\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    with pytest.raises(ParseError):
        execute(code)
