"""Boolean logic tests. These are adapted from the ArnoldC reference
implementation's ``LogicalTest.scala``.
"""

import pytest

from arnoldpy.exceptions import ParseError


def test_false_or_true(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP 0\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION 0\n"
        "CONSIDER THAT A DIVORCE 1\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_true_or_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @NO PROBLEMO\n"
        "CONSIDER THAT A DIVORCE @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_true_or_true(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @NO PROBLEMO\n"
        "CONSIDER THAT A DIVORCE @NO PROBLEMO\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_false_or_falsec(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @I LIED\n"
        "CONSIDER THAT A DIVORCE @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_false_and_true(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @I LIED\n"
        "KNOCK KNOCK @NO PROBLEMO\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_true_and_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @NO PROBLEMO\n"
        "KNOCK KNOCK @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_true_and_true(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @NO PROBLEMO\n"
        "KNOCK KNOCK @NO PROBLEMO\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_true_and_true_and_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION 1\n"
        "KNOCK KNOCK 1\n"
        "KNOCK KNOCK 0\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_true_and_true_and_true(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION 1\n"
        "KNOCK KNOCK 1\n"
        "KNOCK KNOCK 1\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_true_and_true_and_true_and_true_and_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @NO PROBLEMO\n"
        "KNOCK KNOCK @NO PROBLEMO\n"
        "KNOCK KNOCK @NO PROBLEMO\n"
        "KNOCK KNOCK @NO PROBLEMO\n"
        "KNOCK KNOCK @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_false_or_false_or_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @I LIED\n"
        "CONSIDER THAT A DIVORCE @I LIED\n"
        "CONSIDER THAT A DIVORCE @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_false_or_true_and_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @I LIED\n"
        "CONSIDER THAT A DIVORCE @NO PROBLEMO\n"
        "KNOCK KNOCK @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_false_and_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE var\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER var\n"
        "HERE IS MY INVITATION @I LIED\n"
        "KNOCK KNOCK @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND var\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_false_eq_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE varfalse\n"
        "YOU SET US UP @I LIED\n"
        "HEY CHRISTMAS TREE varfalse2\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER varfalse\n"
        "HERE IS MY INVITATION @I LIED\n"
        "YOU ARE NOT YOU YOU ARE ME varfalse2\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND varfalse\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"

def test_true_eq_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE varfalse\n"
        "YOU SET US UP @I LIED\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION @NO PROBLEMO\n"
        "YOU ARE NOT YOU YOU ARE ME varfalse\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_true_eq_true_eq_true(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION @NO PROBLEMO\n"
        "YOU ARE NOT YOU YOU ARE ME @NO PROBLEMO\n"
        "YOU ARE NOT YOU YOU ARE ME @NO PROBLEMO\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_13_eq_13_eq_true(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION 13\n"
        "YOU ARE NOT YOU YOU ARE ME 13\n"
        "YOU ARE NOT YOU YOU ARE ME @NO PROBLEMO\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_13_eq_14_qe_false(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION 13\n"
        "YOU ARE NOT YOU YOU ARE ME 14\n"
        "YOU ARE NOT YOU YOU ARE ME @I LIED\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_1_eq_2_eq_3(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION 1\n"
        "YOU ARE NOT YOU YOU ARE ME 2\n"
        "YOU ARE NOT YOU YOU ARE ME 3\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_13_eq_13_eq_14(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION 13\n"
        "YOU ARE NOT YOU YOU ARE ME 13\n"
        "YOU ARE NOT YOU YOU ARE ME 14\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_1_eq_2(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE one\n"
        "YOU SET US UP 1\n"
        "HEY CHRISTMAS TREE two\n"
        "YOU SET US UP 2\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION one\n"
        "YOU ARE NOT YOU YOU ARE ME two\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_2_qt_1(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE one\n"
        "YOU SET US UP 1\n"
        "HEY CHRISTMAS TREE two\n"
        "YOU SET US UP 2\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION two\n"
        "LET OFF SOME STEAM BENNET one\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "1\n"


def test_1_qt_2(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE one\n"
        "YOU SET US UP 1\n"
        "HEY CHRISTMAS TREE two\n"
        "YOU SET US UP 2\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION one\n"
        "LET OFF SOME STEAM BENNET two\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_3_qt_3(execute):
    code = (
        "IT'S SHOWTIME\n"
        "HEY CHRISTMAS TREE three\n"
        "YOU SET US UP 3\n"
        "HEY CHRISTMAS TREE three2\n"
        "YOU SET US UP 3\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP @NO PROBLEMO\n"
        "GET TO THE CHOPPER result\n"
        "HERE IS MY INVITATION three\n"
        "LET OFF SOME STEAM BENNET three2\n"
        "ENOUGH TALK\n"
        "TALK TO THE HAND result\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    assert execute(code) == "0\n"


def test_detect_faulty_logical_operation(execute):
    code = (
        "IT'S SHOWTIME\n"
        "RIGHT? WRONG! VAR\n"
        "YOU SET US UP @I LIED\n"
        "GET TO THE CHOPPER VAR\n"
        "@I LIED\n"
        "@I LIED\n"
        "CONSIDER THAT A DIVORCE\n"
        "@NO PROBLEMO\n"
        "ENOUGH TALK\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    with pytest.raises(ParseError):
        execute(code)
