"""Read input tests. These are adapted from the ArnoldC reference
implementation's ``InputTest.scala``.
"""

from unittest import mock


def mock_input(val):
    def _call():
        def _input():
            return val
        return _input
    return _call


def test_read_integer(execute):
    code = (
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND \"Input a number:\"\n"
        "HEY CHRISTMAS TREE result\n"
        "YOU SET US UP 0\n"
        "GET YOUR ASS TO MARS result\n"
        "DO IT NOW\n"
        "I WANT TO ASK YOU A BUNCH OF QUESTIONS AND I WANT TO HAVE THEM "
        "ANSWERED IMMEDIATELY\n"
        "TALK TO THE HAND result\n"
        "TALK TO THE HAND \"Bye\"\n"
        "YOU HAVE BEEN TERMINATED\n"
    )
    with mock.patch("builtins.input", new_callable=mock_input(123)):
        assert execute(code) == "Input a number:\n123\nBye\n"