"""Test handling of varied OS newline styles.
"""

def test_newline(execute):
    code = (
        "IT'S SHOWTIME\n"
        "TALK TO THE HAND \"MILK\"\r"
        "TALK TO THE HAND \"BEER\"\r\n"
        "YOU HAVE BEEN TERMINATED"
    )

    assert execute(code) == "MILK\nBEER\n"
