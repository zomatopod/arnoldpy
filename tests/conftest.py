"""Global fixtures for the :mod:`arnoldpy` test suite.
"""

import sys
import contextlib
import io

import pytest

from arnoldpy import parsing
from arnoldpy import python


@pytest.fixture
def parser():
    return parsing.Parser(python.Semantics())


@contextlib.contextmanager
def capture_stdout():
    """Context manager that captures stdout to an :class:`io.StringIO`
    object.
    """

    old_stdout = sys.stdout
    buf = io.StringIO()
    sys.stdout = buf
    try:
        yield buf
    finally:
        sys.stdout = old_stdout


@pytest.fixture(params=["ast", "transpile"])
def execute(request, parser):
    """Return an execution function that compiles an ArnoldC code
    string, runs main, and returns the printed output as a string.

    This fixture is parameterized and will return executors that tests
    both standard Python AST generation and ArnoldC-to-Python source
    transcompilation.
    """
    namespace = {}

    if request.param == "ast":
        def _exec(src):
            with capture_stdout() as buf:
                ast = parser.parse(src).foreign_payload
                code = compile(ast, "<string>", 'exec')
                exec(code, namespace)
                namespace[python.mangle("main")]()
            return buf.getvalue()
    elif request.param == "transpile":
        def _exec(src):
            with capture_stdout() as buf:
                ast = parser.parse(src)
                pysrc = python.unparse(ast)
                code = compile(pysrc, "<string>", 'exec')
                exec(code, namespace)
                namespace[python.mangle("main")]()
            return buf.getvalue()
    return _exec
