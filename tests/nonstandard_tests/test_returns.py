"""More thorough tests for proper return semantics in void and non-void
methods. ArnoldPy enforces a more strict requirement when returning
from non-void methods and asserts that all code paths must return a
value, thus some code that are valid in ArnoldC fails in ArnoldPy.

Deviations are noted in the tests' docstrings.
"""

import pytest

from arnoldpy.exceptions import ParseError


def test_nonvoid_method_no_return(execute):
    """A non-void method with no return statements."""

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)


def test_nonvoid_method_if_return(execute):
    """A return in the body of a conditional should not be accepted as
    properly returning a value. (ArnoldC accepts this; the method
    returns 1).
    """

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
            BECAUSE I'M GOING TO SAY PLEASE value
                I'LL BE BACK 100
            YOU HAVE NO RESPECT FOR LOGIC
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)


def test_nonvoid_method_else_return(execute):
    """A return in the else body of a conditional should not be accepted
    as properly returning a value. (ArnoldC also accepts this.)
    """

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
            BECAUSE I'M GOING TO SAY PLEASE value
            BULLSHIT
                I'LL BE BACK 100
            YOU HAVE NO RESPECT FOR LOGIC
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)


def test_nonvoid_method_ifelse_return(execute):
    """If both bodies of a conditional returns, then the method returns.
    """

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
            BECAUSE I'M GOING TO SAY PLEASE value
                I'LL BE BACK 100
            BULLSHIT
                I'LL BE BACK 1000
            YOU HAVE NO RESPECT FOR LOGIC
        HASTA LA VISTA, BABY
    """
    assert execute(code) == ""


def test_nonvoid_method_ifelse_nested_return_1(execute):
    """Conditional return semantics also applies when they are
    nested. (ArnoldC also accepts this.)
    """

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
            BECAUSE I'M GOING TO SAY PLEASE value
                BECAUSE I'M GOING TO SAY PLEASE value
                    I'LL BE BACK 100
                YOU HAVE NO RESPECT FOR LOGIC
            BULLSHIT
                I'LL BE BACK 1000
            YOU HAVE NO RESPECT FOR LOGIC
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)


def test_nonvoid_method_ifelse_nested_return_2(execute):
    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
            BECAUSE I'M GOING TO SAY PLEASE value
                BECAUSE I'M GOING TO SAY PLEASE value
                    I'LL BE BACK 100
                BULLSHIT
                    I'LL BE BACK 1000
                YOU HAVE NO RESPECT FOR LOGIC
            BULLSHIT
                I'LL BE BACK 1000
            YOU HAVE NO RESPECT FOR LOGIC
        HASTA LA VISTA, BABY
    """
    assert execute(code) == ""


def test_nonvoid_method_while_return(execute):
    """A return in the body of a while loop should not be accepted as
    properly returning a value. (ArnoldC also accepts this.)
    """

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
            LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
            STICK AROUND value
                I'LL BE BACK 100
            CHILL
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)


def test_nonvoid_method_while_if_return(execute):
    """A conditional nested in a while cannot be accepted as returning.
    (ArnoldC also accepts this.)
    """

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
            LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
            STICK AROUND value
                BECAUSE I'M GOING TO SAY PLEASE value
                    I'LL BE BACK 100
                BULLSHIT
                    I'LL BE BACK 1000
                YOU HAVE NO RESPECT FOR LOGIC
            CHILL
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)


def test_void_method_nested_bad_return(execute):
    """An improper return in a deep nest of blocks should still be
    caught (void).
    """

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            STICK AROUND value
                BECAUSE I'M GOING TO SAY PLEASE value
                    BECAUSE I'M GOING TO SAY PLEASE value
                    BULLSHIT
                        I'LL BE BACK 1
                    YOU HAVE NO RESPECT FOR LOGIC
                BULLSHIT
                    TALK TO THE HAND value
                YOU HAVE NO RESPECT FOR LOGIC
            CHILL
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)


def test_nonvoid_method_nested_bad_return(execute):
    """An improper return in a deep nest of blocks should still be
    caught (non-void).
    """

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            GIVE THESE PEOPLE AIR
            STICK AROUND value
                BECAUSE I'M GOING TO SAY PLEASE value
                    BECAUSE I'M GOING TO SAY PLEASE value
                    BULLSHIT
                        I'LL BE BACK
                    YOU HAVE NO RESPECT FOR LOGIC
                BULLSHIT
                    TALK TO THE HAND value
                YOU HAVE NO RESPECT FOR LOGIC
            CHILL
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)
