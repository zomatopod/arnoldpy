"""Test variable naming and scope accessibility. Most of these are
instances where ArnoldPy deviates from standard ArnoldC for as some of
these scenarios actually crash the current Java implementation last I
tried. (Always declare all your variables at the beginning of the
method, and especially not within a conditional or loop!)

Given the choice of slavish adherence or pragmatism, I opted for the
latter.

Deviations are noted in the tests' docstrings.
"""

import pytest

from arnoldpy.exceptions import ParseError


def test_inner_scope_declaration(execute):
    """If and while blocks introduce new scopes. Variables can have
    the same name in another, lateral scope. (Uncaught exception at
    compile with ArnoldC.)
    """

    code = """
        IT'S SHOWTIME
            HEY CHRISTMAS TREE loopVar
            YOU SET US UP 3
            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                HEY CHRISTMAS TREE var1
                YOU SET US UP 1100
                TALK TO THE HAND var1
            YOU HAVE NO RESPECT FOR LOGIC
            STICK AROUND loopVar
                HEY CHRISTMAS TREE var1
                YOU SET US UP 300
                GET TO THE CHOPPER loopVar
                    HERE IS MY INVITATION loopVar
                    GET DOWN 1
                ENOUGH TALK
                TALK TO THE HAND var1
            CHILL
        YOU HAVE BEEN TERMINATED
    """
    assert execute(code) == "1100\n300\n300\n300\n"


def test_outer_accessibility_from_inner_scope(execute):
    """Outer variables should be accessible in an inner scope.
    (Uncaught exception at compile with ArnoldC.)
    """

    code = """
        IT'S SHOWTIME
            HEY CHRISTMAS TREE var1
            YOU SET US UP 1100
            HEY CHRISTMAS TREE loopVar
            YOU SET US UP 3

            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                TALK TO THE HAND var1
            YOU HAVE NO RESPECT FOR LOGIC

            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
            BULLSHIT
                TALK TO THE HAND var1
            YOU HAVE NO RESPECT FOR LOGIC

            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                GET TO THE CHOPPER var1
                    HERE IS MY INVITATION var1
                    GET DOWN 100
                ENOUGH TALK
                TALK TO THE HAND var1
            BULLSHIT
            YOU HAVE NO RESPECT FOR LOGIC

            STICK AROUND loopVar
                GET TO THE CHOPPER loopVar
                    HERE IS MY INVITATION loopVar
                    GET DOWN 1
                ENOUGH TALK
                GET TO THE CHOPPER var1
                    HERE IS MY INVITATION var1
                    HE HAD TO SPLIT 10
                ENOUGH TALK
                TALK TO THE HAND var1
            CHILL

        YOU HAVE BEEN TERMINATED
    """
    assert execute(code) == "1100\n1000\n100\n10\n1\n"


def test_inner_accessibility_from_outer_scope(execute):
    """Variables defined in an inner scope should not be accessible from
    the outside. (ArnoldC will compile this, but the program will
    crash at runtime.)
    """

    code = """
        IT'S SHOWTIME
            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                HEY CHRISTMAS TREE var1
                YOU SET US UP 1100
                TALK TO THE HAND var1
            YOU HAVE NO RESPECT FOR LOGIC
            TALK TO THE HAND var1
        YOU HAVE BEEN TERMINATED
    """
    with pytest.raises(ParseError):
        execute(code)


def test_same_name_from_inner_scope(execute):
    """A variable with the same name as that defined in an inner scope
    is legal. (Not valid ArnoldC.)
    """

    code = """
        IT'S SHOWTIME
            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                HEY CHRISTMAS TREE var1
                YOU SET US UP 1100
                TALK TO THE HAND var1
            YOU HAVE NO RESPECT FOR LOGIC
            HEY CHRISTMAS TREE var1
            YOU SET US UP 900
            TALK TO THE HAND var1
        YOU HAVE BEEN TERMINATED
    """
    assert execute(code) == "1100\n900\n"


def test_inner_scope_shadowing(execute):
    """An inner scope variable cannot shadow a name from an outer
    scope.
    """

    code = """
        IT'S SHOWTIME
            HEY CHRISTMAS TREE var1
            YOU SET US UP 900
            TALK TO THE HAND var1
            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                HEY CHRISTMAS TREE var1
                YOU SET US UP 1100
                TALK TO THE HAND var1
            YOU HAVE NO RESPECT FOR LOGIC
        YOU HAVE BEEN TERMINATED
    """
    with pytest.raises(ParseError):
        execute(code)


def test_variable_same_name_as_method(execute):
    """A variable can have the same name as a defined method.
    (Java and thus ArnoldC allow this by default, but it takes extra
    work to make this legal in Python, thus why we specifically test
    it.)
    """

    code = """
        IT'S SHOWTIME
            HEY CHRISTMAS TREE endofdays
            YOU SET US UP 666
            DO IT NOW endofdays endofdays
        YOU HAVE BEEN TERMINATED

        LISTEN TO ME VERY CAREFULLY endofdays
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            TALK TO THE HAND value
        HASTA LA VISTA, BABY
    """
    assert execute(code) == "666\n"


def test_duplicate_parameters(execute):
    """Duplicate parameter names are not allowed."""

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0 0
        YOU HAVE BEEN TERMINATED

        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)


def test_duplicate_parameters_and_variables(execute):
    """Variables with the same name as parameters are not allowed."""

    code = """
        IT'S SHOWTIME
            DO IT NOW method 0 0
        YOU HAVE BEEN TERMINATED

        LISTEN TO ME VERY CAREFULLY method
            I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE value
            HEY CHRISTMAS TREE value
            YOU SET US UP 0
        HASTA LA VISTA, BABY
    """
    with pytest.raises(ParseError):
        execute(code)
