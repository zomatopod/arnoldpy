"""A little more coverage for testing calling methods with arguments.
"""

import pytest

# Note that these are parse errors in ArnoldC, but runtime errors in
# ArnoldPy.

def test_too_few_arguments(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW method\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY method\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE arg1\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE arg2\n"
        "HASTA LA VISTA, BABY\n"
    )
    with pytest.raises(TypeError):
        execute(code)


def test_too_many_arguments(execute):
    code = (
        "IT'S SHOWTIME\n"
        "DO IT NOW method 1 2 3\n"
        "YOU HAVE BEEN TERMINATED\n"
        "LISTEN TO ME VERY CAREFULLY method\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE arg1\n"
        "I NEED YOUR CLOTHES YOUR BOOTS AND YOUR MOTORCYCLE arg2\n"
        "HASTA LA VISTA, BABY\n"
    )
    with pytest.raises(TypeError):
        execute(code)
