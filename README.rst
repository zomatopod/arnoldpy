ArnoldPy
========

"`What is this?`_"

    It's a reimplementation of ArnoldC_ in Python.

"`What does it do?`_"

    It can directly run an ArnoldC script by compiling it into a Python code
    object and then executing it, and it can translate an ArnoldC program into
    an equivalent Python source.

"`Wait, hold up... so this is another version of a language that's comprised
entirely of Arnold Schwarzenegger movie quotes?
<https://www.youtube.com/watch?v=GFUAH9wb91Y>`_"

    Pretty much, with some `differences`_.

"... really? *Really?*"

    Yes_.

.. _What is this?: https://www.youtube.com/watch?v=AzllNSKItYw
.. _ArnoldC: http://lhartikk.github.io/ArnoldC/
.. _What does it do?: https://www.youtube.com/watch?v=nP2xnAMQ3HM
.. _Yes: https://www.youtube.com/watch?v=CpKGRDtC8WQ



Usage
-----

An online testbed of ArnoldPy can be found at: http://arnoldpy.heroku.com.

Local installations of ArnoldPy requires Python 3.4+. Install it from the source
directory with::

    $ python3 setup.py install --user

Then, it's simply a matter of running ``arnoldpy``::

    $ ~/.local/bin/arnoldpy im_back.arnoldc

Global installation may be done by omitting the ``--user`` setup flag.

It's also possible to run ArnoldPy directly without setuptools installation
using Python's directory execution functionality. However, because
``requirements.txt`` dependencies must still be met, this method is of reduced
utility::

    $ pip3 install --user -r requirements.txt
    $ python3 arnoldpy im_back.arnoldc

View available command-line options with ``--help``.



.. _differences:

Differences from ArnoldC
------------------------

ArnoldPy is generally compatible with programs written for ArnoldC. However,
there are certain instances where it diverges from standard ArnoldC behavior,
although it still passes all tests (plus `fizzbuzz`_) from the Java reference
implementation. These differences mostly stem from the fact that Python is, in
fact, not Java.

Put it this way: while not all ArnoldPy is valid ArnoldC, all ArnoldC... is not
necessarily valid ArnoldPy either. Well, uh...

Well, `crap`_.

* Variables aren't 16-bit integers but Python's native *int*. In Python 3, that
  means numbers can have arbitrary precision. Big numbers for big strong men
  (and women)!

* ArnoldPy enforces stricter return semantics for non-void methods. The
  following is legal in ArnoldC (``beer`` implicitly returns 1)::

        IT'S SHOWTIME
            HEY CHRISTMAS TREE milk
            YOU SET US UP 0
            GET YOUR ASS TO MARS milk
            DO IT NOW beer
            TALK TO THE HAND milk
        YOU HAVE BEEN TERMINATED
        LISTEN TO ME VERY CAREFULLY beer
            GIVE THESE PEOPLE AIR
            BECAUSE I'M GOING TO SAY PLEASE @I LIED
            I'LL BE BACK 100
            YOU HAVE NO RESPECT FOR LOGIC
        HASTA LA VISTA, BABY

  but is an error in ArnoldPy. (Note that if the return condition is
  instead ``@NO PROBLEMO``, the program will still be rejected because
  ArnoldPy is dumb.)

* Variable scopes tend to work as you expect (a facsimile of Java). These fail
  in various ways under ArnoldC but are valid in ArnoldPy::

        IT'S SHOWTIME
            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                HEY CHRISTMAS TREE milk
                YOU SET US UP 1100
            YOU HAVE NO RESPECT FOR LOGIC
            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                HEY CHRISTMAS TREE milk
                YOU SET US UP 1100
            YOU HAVE NO RESPECT FOR LOGIC
        YOU HAVE BEEN TERMINATED

        IT'S SHOWTIME
            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                HEY CHRISTMAS TREE milk
                YOU SET US UP 1100
            YOU HAVE NO RESPECT FOR LOGIC
            HEY CHRISTMAS TREE milk
            YOU SET US UP 1100
        YOU HAVE BEEN TERMINATED

  Note that this is still an error::

        IT'S SHOWTIME
            HEY CHRISTMAS TREE milk
            YOU SET US UP 1100
            BECAUSE I'M GOING TO SAY PLEASE @NO PROBLEMO
                HEY CHRISTMAS TREE milk
                YOU SET US UP 1100
            YOU HAVE NO RESPECT FOR LOGIC
        YOU HAVE BEEN TERMINATED

  (It's not *that* dumb.)

* Method name resolution is not done during parsing, so invalid calls to
  methods (bad names or incorrect numbers of arguments) are runtime errors.
  This may change, but at some point, you gotta ask yourself: "Is Python meant
  to be like a statically-typed language, or is it not?"[*]_

* The single best (and only) improvement this implementation has:
  whitespace is allowed before the first program statement. `HELL. YES.`_

* Probably a bunch of more nuances that test coverage fails to pick up,
  because ArnoldPy is pretty dumb.

.. _fizzbuzz: https://gist.github.com/georg/9224355
.. _crap: https://www.youtube.com/watch?v=c4psKYpfnYs
.. _HELL. YES.: https://www.youtube.com/watch?v=ItzslynRhwg
.. [*] `Likely answer. <https://www.youtube.com/watch?v=RS81SCTw1JE>`_



License
-------

ArnoldPy is licensed under the Manly International Testosterone (MIT) License.

(I lied, since that doesn't exist. But it *is* under the Massachusetts Institute
of Technology License, which seems close enough.)



Leave Anything Else?
--------------------

Just bodies.
